$(document).ready(function() {
  var navLinks = $('.nav-link');

  var friendsInStepOne = [],
      friendsInStepTwo = [],
      friendsInStepThree = [],
      friendsInStepFour = [],
      friendsInStepFive = [],
      friendsInStepSix = [],
      friendsInStepSeven = [];

  getFriends();

  navLinks.on('click', function(e) {
    e.preventDefault();
    $('.active').removeClass('active');
    $(this).parent().addClass('active');

    console.log($(this).text);

    scrollContent($(this)['0'].text)
  });

  function scrollContent(link) {
    switch (link) {
      case 'Baby Step 1':
        $('.content').animate({top: '0px'}, 'slow');
        break;
      case 'Baby Step 2':
        $('.content').animate({top: '-300px'}, 'slow');
        break;
      case 'Baby Step 3':
        $('.content').animate({top: '-600px'}, 'slow');
        break;
      case 'Baby Step 4':
        $('.content').animate({top: '-900px'}, 'slow');
        break;
      case 'Baby Step 5':
        $('.content').animate({top: '-1200px'}, 'slow');
        break;
      case 'Baby Step 6':
        $('.content').animate({top: '-1500px'}, 'slow');
        break;
      case 'Baby Step 7':
        $('.content').animate({top: '-1800px'}, 'slow');
        break;
    }
  }

  function getFriends() {
    $.ajax({
      url: "./assets/javascripts/baby-steps.json",
      context: document.body,
      success: function(data) {
        $.each(data.friends, function(i) {
          sortFriends(data.friends[i]);
        })

        insertFriends(1, friendsInStepOne);
        insertFriends(2, friendsInStepTwo);
        insertFriends(3, friendsInStepThree);
        insertFriends(4, friendsInStepFour);
        insertFriends(5, friendsInStepFive);
        insertFriends(6, friendsInStepSix);
        insertFriends(7, friendsInStepSeven);
      }
    });
  }

  function insertFriends(id, array) {
    if(array.length <= 0) {
      $('#babyStep' + id + ' .friends').css('display', 'none');
    } else if (array.length == 1) {
      $('#babyStep' + id + ' .friends').html(`
        <a href=''>${ array[0].firstName } ${ array[0].lastName }</a> is also in Baby Step ${id}.
      `);
    } else if (array.length == 2) {
      $('#babyStep' + id + ' .friends').html(`
        <a href=''>${ array[0].firstName } ${ array[0].lastName }</a> and <a href=''>${ array[1].firstName } ${ array[1].lastName }</a> are also in Baby Step ${id}.
      `);
    } else if (array.length >= 3) {
      $('#babyStep' + id + ' .friends').html(`
        <a href=''>${ array[0].firstName } ${ array[0].lastName }</a>, <a href=''>${ array[1].firstName } ${ array[1].lastName }</a> and ${array.length - 2 } other friends are also in Baby Step ${id}.
      `);
    }
  }

  function sortFriends(friend) {
    switch (friend.babyStep) {
      case 1:
        friendsInStepOne.push(friend);
        break;
        case 2:
          friendsInStepTwo.push(friend);
          break;
          case 3:
            friendsInStepThree.push(friend);
            break;
            case 4:
              friendsInStepFour.push(friend);
              break;
              case 5:
                friendsInStepFive.push(friend);
                break;
                case 6:
                  friendsInStepSix.push(friend);
                  break;
                  case 7:
                    friendsInStepSeven.push(friend);
                    break;
      default:

    }
  }

})
